//Kata1 Add
function add (a,b) {return (a + b)}
console.log("kata1 " + add (10,42));

//Kata2 Multiply
function multiply (a,b){
    let sum = 0;
    if ((a==0)||(b==0)) {return 0}
    for (let i = 0; i < b; i++) {sum = add(sum,a)}
return sum;
}
console.log("Kata2 " + multiply(7,9));

//alternate solution
//function multiply2(a,b){return("a").repeat(a).repeat(b).length}

//Kata3 Powers

function power (a,b){
    let sum = 1;
    if((a==0)&&(b==0)) {return 1} 
    else  if (a==0)   {return 0}
    for (let i = 0; i < b; i++) {sum = multiply(sum,a)}
return sum;
}
console.log("Kata3 " + power(2,5));

//Kata4 Factorial
function factorial (num){
let arr =[]
let count = num;
for (let i = 0; i < num;i++){
    arr.push(count);
    count = count-1;
}   
    let sum = multiply(arr[0],arr[1])
    for (let i = 2; i < num; i++){
    sum = multiply(sum,arr[i]);
   }
return sum;
}

console.log("kata4 " + factorial(5));

//Kata5 Fibonacci
function fibonacci (num){
    let n1 = 0;
    let n2 = 1;
    let sum = 0
    for (let i = 0; i < num; i++){
    sum = add(n1,n2);
    n2 = n1;
    n1 = sum;
    }
return sum;
}
console.log("kata5 " + fibonacci(19));